## BIODATA

**Nama    :   Faisal Dzulfikar**

**Alamat  :   Sanggar Indah Banjaran Blok H5 No 17**

**Email   :   faisd405@gmail.com**

## Daftar Tugas
1. Tugas 1 - HTML
2. Tugas 2 - CSS
3. Tugas 3 - CSS With Bootstrap
4. Tugas 4 - Javascript
5. Tugas 5 - Javascript DOM
6. Tugas 6 - GIT
7. Tugas 7 - PHP String Array
8. Tugas 8 - PHP Looping Function Control Flow
9. Tugas 9 - OOP PHP
10. Tugas 10 - Belajar SQL
11. Tugas 11 - Belajar ERD